#ifdef UNIT_TESTING_ENABLED

extern "C" {
#include "crc32.c"
}

#include <gtest/gtest.h>

uint8_t data[256] = {0};

#define CRC32_EXPECTED_RESULT 0x29058C73

crc32_ctx_t crc_ctx;


TEST(CRC_Test, crc32 )
{

    for(int i = 0; i < 256; i++)
    {
        data[i] = i;
    }

    crc32_init(&crc_ctx);
    crc32_update(&crc_ctx, data, 256);
    uint32_t crc_result = crc32_result(&crc_ctx);

    ASSERT_EQ(crc_result,CRC32_EXPECTED_RESULT); //No partitions
}





#endif